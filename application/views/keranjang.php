<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Produk Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Bootstrap glyphicon CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/portfolio-item.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/toastr.min.css');?>" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">OlShopKu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- <input type="text" class="form-control input-sm" placeholder="Cari"> -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="<?php echo base_url('keranjang'); ?>"><span class="glyphicon glyphicon-shopping-cart"></span>  <?php echo $this->cart->total_items(); ?> Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><?php echo @$email;?></a>  </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('login/logout'); ?>">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container w-100 p-3">

      <!-- Portfolio Item Heading -->
      <h1 class="my-5"><span class="glyphicon glyphicon-shopping-cart"></span> Shop Cart
      </h1>

      <table class="table w-100 p-3 table-bordered" >
          <thead>
            <tr>
              <th>ID Barang</th>
              <th>Produk</th>
              <th>Harga</th>
              <th>Jumlah</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($this->cart->contents() as $item): ?>

            <tr>
              <td><?php echo $item['id'] ?></td>
              <td><?php echo $item['name'] ?></td>
              <td align='right'><?php echo $harga=number_format($item['price'],0,",","."); ?></td>
              <td align='right'><?php echo $item['qty'] ?></td>
              <td align='right'><?php echo $item['subtotal'] ?></td>
                    <!-- FITUR TAMBAHAN REMOVE PER ITEM -->
              <td align='center' class="none"> <button type="button" onclick="hapus_item('<?php echo $item['rowid'] ?>');" class="btn btn-danger">Remove</button> </td>
            </tr>

            <?php endforeach; ?>

          </tbody>
          <tfoot>

                        <tr>
                            <td align='right' colspan="4">Total Bayar : </td>
                              <td align='right'><?php echo $this->cart->total(); ?></td>
                        </tr>
          </tfoot>
        </table>

    </div>
    <!-- /.container -->
    <div class="text-center">
    <h4> <a href="<?php echo base_url('keranjang/clear_keranjang') ?>"><button type="button" class="btn btn-danger center-block"><span class="glyphicon glyphicon glyphicon-refresh"></span> Clear Cart</button></a>
        <button type="button" onclick="location.href='home'" class="btn btn-info center-block"><span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping</button>
        <a href="#"><button type="button" onclick="checkout();" class="btn btn-success center-block"><span class="glyphicon glyphicon glyphicon-ok"></span> Check Out</button></a>
    </h4>
  </div>
  <div class="text-center">
    <h4>
      <a href="#"><button type="button" onclick="location.href='pesanan'" class="btn btn-warning center-block"><span class="glyphicon glyphicon-list-alt"></span> List Order</button></a>
  </h4>
</div>
    <!-- Footer -->
    <footer class="py-5 bg-dark" style="margin-top:40px;">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

      <!-- -Checkout Modal- -->
  <div class="modal fade" id="checkoutModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Pembayaran</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        	<div align="middle" class="form-group">
          <p><b>Total Pembayaran</b></p>
          Rp. <input type='text' style='font-weight: bold;background-color:#E8EAF6;border: none; text-align:center' value="<?php echo number_format($this->cart->total(),0,",","."); ?>" readonly>
          </div>

        <div class="form-group" style="margin-top:50px;">
              <label><b>Cara Bayar dengan Transfer Bank</b></label>
        </div>

          <div class="form-group">
          <label>Pembeli dapat melakukan pembayaran melalui transfer ke salah satu rekening OlShopKu di bawah ini:</label>
          <ul>
              <strong>
              <li>BCA (No. Rek: xxx xxx xxxx)</li>
              <li>Mandiri (No. Rek: xxxx xxx xxx xxx)</li>
              <li>BRI (No. Rek: xxx xxx xxx xxx xxx)</li>
              <li>Bank Syariah Mandiri (No Rek: xxx xxx xxxx)</li>
            </strong>
          </ul>

          </div>


        </div>
        <div class="modal-footer">
          <button type="button" onclick="location.reload();" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- -end Checkout Modal- -->


    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/toastr.js');?>"></script>


  </body>

</html>

<script>
function hapus_item(rowid){
  $.ajax({
    url: '<?php echo base_url('keranjang/remove_item')?>',
    type: 'POST',
    data: {rowid: rowid}
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");

  });


}

function checkout(){
  $.ajax({
    url: '<?php echo base_url('keranjang/checkout')?>',
    type: 'POST',
    data: {rowid: '1'},
    success: function(data)
             {
               // alert(data);

               if (data=='Keranjang Kosong'){alert(data)}else{toastr.success('Pesanan anda akan dikonfirmasi', 'Berhasil!');$('#checkoutModal').modal('show');}
               // setTimeout(function(){ location.reload(); }, 1000);
               // toastr.success('telah ditambahkan dalam keranjang', 'Berhasil!')
             }
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });


}


</script>

            <!-- JavaScript Intergram -->
<script>
    window.intergramId = "496751007";
    window.intergramCustomizations = {
        titleClosed: 'Kontak Kami',
        titleOpen: 'OlShopKu',
        introMessage: 'Halo, ada yang bisa saya bantu?',
        autoResponse: 'Harap tunggu beberapa saat, salah satu Admin kami akan membantu anda.',
        autoNoResponse: 'Mohon maaf mungkin admin kami sedang sibuk ' +
                        'tunggulah beberapa saat lagi',
        mainColor: "#1976D2", // Can be any css supported color 'red', 'rgb(255,87,34)', etc
        alwaysUseFloatingButton: false // Use the mobile floating button also on large screens
    };
</script>
<script id="intergram" type="text/javascript" src="https://www.intergram.xyz/js/widget.js"></script>
