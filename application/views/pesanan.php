<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Produk Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Bootstrap glyphicon CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/portfolio-item.css');?>" rel="stylesheet">

  </head>

  <body>

    <!-- Modal -->
  <div class="modal fade" id="modal_detail_pesanan" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detail Pesanan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</div>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">OlShopKu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- <input type="text" class="form-control input-sm" placeholder="Cari"> -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('keranjang'); ?>"><span class="glyphicon glyphicon-shopping-cart"></span>  <?php echo $this->cart->total_items(); ?> Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><?php echo @$email;?></a>  </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('login/logout'); ?>">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container w-100 p-3">

      <!-- Portfolio Item Heading -->
      <h1 class="my-5"><span class="glyphicon glyphicon-list-alt"></span> List Order
      </h1>

      <table class="table w-100 p-3 table-bordered" >
          <thead>
            <tr align='center'>
              <th>No Transaksi</th>
              <th>Jumlah</th>
              <th>Total Pembayaran</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($item as $data_item): ?>

            <tr>
              <td align='center'><?php echo $data_item->no_transaksi ?></td>
              <td align='center'><?php echo $data_item->jumlah_item ?></td>
              <td align='right'>Rp.<?php echo $harga=number_format($data_item->total_bayar,0,",","."); ?></td>
              <td align='center'><?php echo $data_item->status ?></td>
                    <!-- FITUR TAMBAHAN REMOVE PER ITEM -->
              <td align='center' class="none"> <button type="button" class="btn btn-primary" onclick="tampil_detail('<?php echo $data_item->no_transaksi ?>')">Detail</button> </td>
            </tr>

            <?php endforeach; ?>

          </tbody>
          <tfoot>

          </tfoot>
        </table>

    </div>
    <!-- /.container -->
    <div class="text-center">
    <h4>
        <button type="button" onclick="location.href='home'" class="btn btn-info center-block"><span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping</button>
    </h4>
  </div>
    <!-- Footer -->
    <footer class="py-5 bg-dark" style="margin-top:150px;">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>

  </body>

</html>

<script>
function hapus_item(rowid){
  $.ajax({
    url: '<?php echo base_url('keranjang/remove_item')?>',
    type: 'POST',
    data: {rowid: rowid}
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });


}

function checkout(){
  $.ajax({
    url: '<?php echo base_url('keranjang/checkout')?>',
    type: 'POST',
    data: {rowid: '1'}
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

}


</script>


<script type="text/javascript">
	function tampil_detail(no_transaksi) {
		$.ajax({
      url : '<?php echo base_url('pesanan/tampil_detail')?>',
			type: 'post',
      data: {no_transaksi: no_transaksi},

			success: function(hasil) {
				$response = $(hasil);
        var isi = $response.html();
        $('#modal_detail_pesanan .modal-body').html(isi);
				// // ambil data dari url admin/edit_barang?id_barang=(sekian)
				// var nama_barang = $response.filter('#nama_barang').text();
				// var harga = $response.filter('#harga').text();
				// var jumlah = $response.filter('#jumlah').text();
				// // menampilkan ke modal
				// $('#nama_barang').val(nama_barang);
				// $('#harga').val(harga);
				// $('#jumlah').val(jumlah);
        $('#modal_detail_pesanan').modal('show');
        // alert(hasil);

			}
		});
	}
</script>


<!-- JavaScript Intergram -->
<script>
window.intergramId = "496751007";
window.intergramCustomizations = {
titleClosed: 'Kontak Kami',
titleOpen: 'OlShopKu',
introMessage: 'Halo, ada yang bisa saya bantu?',
autoResponse: 'Harap tunggu beberapa saat, salah satu Admin kami akan membantu anda.',
autoNoResponse: 'Mohon maaf mungkin admin kami sedang sibuk ' +
            'tunggulah beberapa saat lagi',
mainColor: "#1976D2", // Can be any css supported color 'red', 'rgb(255,87,34)', etc
alwaysUseFloatingButton: false // Use the mobile floating button also on large screens
};
</script>
<script id="intergram" type="text/javascript" src="https://www.intergram.xyz/js/widget.js"></script>
