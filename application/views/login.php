
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/login.css');?>" rel="stylesheet">
  </head>

  <body class="text-center">
    <form action="<?php echo base_url('login/aksi_login'); ?>" method="post" class="form-signin" id="formLogin">
      <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Halaman Login</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="text" id="username" name='email' class="form-control" placeholder="Email address" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="password" name='password' class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3">
      </div>
      <button class="btn btn-lg btn-primary btn-block" id="buttonSubmit" type="submit">Login</button>
      <a href="<?php echo base_url('login/sign_up');?>">Belum punya akun? Klik disini</a>
      <p class="mt-5 mb-3 text-muted"></p>
      <a class="btn btn-lg btn-danger btn-block"href="<?php echo base_url('login/login_g');?>">Google</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2018</p>

    </form>
  </body>
</html>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>

<script>

// $("#buttonSubmit").click(function(event) {
//   $.ajax({
//     url: '<?php echo base_url('login/aksi_login') ?>',
//     type: 'POST',
//     data: $("#formLogin").serialize(),
//   	success: function(data)
//              {
//                // alert(data);
//   						 if(data=='member'){
//                  window.location.href = 'home';
//   					           }
//                        else if(data=='admin'){alert('admin');}
//                        else{}
//
//           }
//       })
//       .done(function() {
//         console.log("success");
//       })
//       .fail(function() {
//         console.log("error");
//       })
//       .always(function() {
//         console.log("complete");
//       });
// });

function tes(){
$.ajax({
  url: '<?php echo base_url('login/tes') ?>',
  type: 'POST',
  data: {param1: 'value1'},
  success: function(data)
           {
             alert(data);


        }
})
.done(function() {
  console.log("success");
})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});


}

</script>


<!-- JavaScript Intergram -->
<script>
window.intergramId = "496751007";
window.intergramCustomizations = {
titleClosed: 'Kontak Kami',
titleOpen: 'OlShopKu',
introMessage: 'Halo, ada yang bisa saya bantu?',
autoResponse: 'Harap tunggu beberapa saat, salah satu Admin kami akan membantu anda.',
autoNoResponse: 'Mohon maaf mungkin admin kami sedang sibuk ' +
            'tunggulah beberapa saat lagi',
mainColor: "#1976D2", // Can be any css supported color 'red', 'rgb(255,87,34)', etc
alwaysUseFloatingButton: false // Use the mobile floating button also on large screens
};
</script>
<script id="intergram" type="text/javascript" src="https://www.intergram.xyz/js/widget.js"></script>
