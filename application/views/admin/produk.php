<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin OlShopku</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('assets/css/sb-admin.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('assets/css/sweetalert.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('assets/js/dataTables.bootstrap4.css') ?>" rel='stylesheet' type='text/css' />

  <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
  <!-- Custom fonts for this template-->
  <!-- <link href="vendor/bootstrap/css/font-awesome.css" rel="stylesheet" type="text/css"> -->
  <!-- <link href="vendor/bootstrap/css/fontawesome.min.css" rel="stylesheet" type="text/css"> -->

  <!-- Page level plugin CSS-->
  <!-- <link href="vendor/bootstrap/js/dataTables.bootstrap4.css" rel="stylesheet"> -->
  <!-- Custom styles for this template-->
  <!-- <link href="vendor/bootstrap/css/sb-admin.css" rel="stylesheet"> -->
  <!-- Sweetalert-->
  <!-- <link href="assets/css/sweetalert.css" rel="stylesheet"> -->

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php $this->load->view('template/navbar.php'); ?>
  <!-- Navigation-->

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Produk</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Produk</div>
          <div class="card-body">
            <button type="button" class="btn btn-primary" style="margin-bottom:15px;" data-toggle="modal" data-target="#tambahModal"><span class="glyphicon glyphicon-plus-sign"> Tambah</span></button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID Barang</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID Barang</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php foreach ($item as $data_item): ?>
                    <tr>
                      <td><?php echo $data_item->id_barang ?></td>
                      <td align='center'><?php echo $data_item->nama ?></td>
                      <td align='right'>Rp.<?php echo $harga=number_format($data_item->harga,0,",","."); ?></td>
                      <td align='center'><?php echo $data_item->gambar ?></td>
                      <td align='center' class="none">
                        <button type="button" class="btn btn-warning" onclick="tampilUbahData('<?php echo $data_item->id_barang ?>')"><span class="glyphicon glyphicon-pencil"></span></button>
                        <button type="button" class="btn btn-danger" onclick="hapusData('<?php echo $data_item->id_barang ?>')"><span class="glyphicon glyphicon-trash"></span></button>
                      </td>

                    </tr>

                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Your Website 2018</small>
          </div>
        </div>
      </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>

      <!-- Tambah Produk Modal -->

      <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"> <b>Tambah Data Produk </b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

              <form action="<?php echo base_url('admin/Produk/do_upload') ?>" method="post" id="form_tambah_produk" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Nama Produk:</label>
                  <input type="text" class="form-control" id="nama" name="nama" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')">
                </div>

                <div class="form-group">
                  <label for="recipient-name" class="control-label">Deskripsi Produk:</label>
                  <textarea rows='3' class="form-control" id="deskripsi" name="deskripsi" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
                </div>

                <div style="margin-top:50px;" class="form-group">
                  <label for="message-text" class="control-label">Harga :</label>
                  <!-- <input type="text" class="form-control" id="harga" name="harga"> -->
                  <span class="input-group-addon">Rp.</span>
                  <input type="number" pattern="^\d+(\.|\,)\d{2}$" value="1000" min="0" step="1000.00" data-number-to-fixed="2" data-number-stepfactor="100" id="harga" name="harga" />
                </div>

                <div style="margin-top:30px;" class="form-group">
                  <label>Upload Gambar</label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <span class="btn btn-default btn-file">
                        Browse… <input type="file" id="imgInp" name="userfile">
                      </span>
                    </span>
                    <input type="hidden" class="form-control" id="gambar" name="gambar"  readonly>
                  </div>
                  <img style="max-width: 400px;max-height: 300px;" id='img-upload'/>
                </div>
                <!-- <input type="submit" value="upload" />

              </form> -->

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" onclick="" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </div>
      </div>
    </form>

    <!-- Modal -->

    <!-- Modal Ubah -->
    <div class="modal fade" id="ubahModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> <b>Data Produk </b></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">

            <form action="<?php echo base_url('admin/Produk/ubahData');?>" method="post" id="form_ubah_produk" enctype="multipart/form-data">
              <input type="text" class="form-control" id="id_barang" name="id_barang" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" hidden>

              <div class="form-group">
                <label for="recipient-name" class="control-label">Nama Produk:</label>
                <input type="text" class="form-control" id="ubah_nama" name="ubah_nama" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')">
              </div>

              <div class="form-group">
                <label for="recipient-name" class="control-label">Deskripsi Produk:</label>
                <textarea rows='3' class="form-control" id="ubah_deskripsi" name="ubah_deskripsi" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
              </div>

              <div style="margin-top:50px;" class="form-group">
                <label for="message-text" class="control-label">Harga :</label>
                <!-- <input type="text" class="form-control" id="harga" name="harga"> -->
                <span class="input-group-addon">Rp.</span>
                <input type="number" pattern="^\d+(\.|\,)\d{2}$" value="1000" min="0" step="1000.00" data-number-to-fixed="2" data-number-stepfactor="100" id="ubah_harga" name="ubah_harga" />
              </div>

              <div style="margin-top:30px;" class="form-group">
                <label>Upload Gambar</label>
                <div class="input-group">
                  <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                      Browse… <input type="file" id="imgInp" name="userfile">
                    </span>
                  </span>
                  <input type="text" class="form-control" id="ubah_gambar" name="ubah_gambar"  readonly>
                  <input type="text" class="form-control" id="ubah_gambar2" name="ubah_gambar2"  readonly>
                </div>
                <img style="max-width: 400px;max-height: 300px;" id='ubah_img-upload'/>
              </div>
              <!-- <input type="submit" value="upload" />

            </form> -->

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" onclick="" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  </form>

  <!-- Modal -->

  <!-- Logout Modal-->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih logout untuk keluar.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url('login') ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Bootstrap core JavaScript-->
  <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <script src="<?php echo base_url('/assets/js/jquery.min.js') ?>"></script>

  <script src="<?php echo base_url('/assets/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.dataTables.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/dataTables.bootstrap4.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/sb-admin.min.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/sb-admin-datatables.min.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/sweetalert.min.js') ?>"></script>

  <script>


  $( "#form_tambah_produk" ).submit(function( event ) {

    // event.preventDefault();
    swal("Berhasil!", "Data telah disimpan. ", "success");
    setTimeout(function(){ location.reload(); }, 1000);
  });



  // Preview Gambar
  $(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

      var input = $(this).parents('.input-group').find(':text'),
      log = label;

      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }

    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#img-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp").change(function(){
      readURL(this);
    });
  });
  // End Preview Gambar


  // ===Function Ubah Produk =======

  function tampilUbahData(id_barang){

    $.ajax({
      url : '<?php echo base_url('admin/Produk/tampilUbahData')?>',
      type: 'post',
      data: {id_barang: id_barang},

      success: function(hasil) {
        $response = $(hasil);
        // // ambil data dari url admin/edit_barang?id_barang=(sekian)
        var id_barang = $response.filter('#id_barang').text();
        var nama = $response.filter('#nama').text();
        var deskripsi = $response.filter('#deskripsi').text();
        var harga = $response.filter('#harga').text();
        var gambar = $response.filter('#gambar').text();
        // menampilkan ke modal
        $('#id_barang').val(id_barang);
        $('#ubah_nama').val(nama);
        $('#ubah_deskripsi').val(deskripsi);
        $('#ubah_harga').val(harga);
        $('#ubah_gambar2').val(gambar);
        document.getElementById("ubah_img-upload").src=gambar;
        $('#ubahModal').modal('show');
      }
    });
  }
  // === End Function Hapus Produk =======

  function hapusData(id_barang){
    swal({
      title: "Apakah anda yakin?",
      text: "Data akan hilang secara permanen!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Ya, hapus!",
      closeOnConfirm: false
    },
    function(){
      swal("Terhapus!", "Data telah dihapus.", "success");
      $.ajax({
        url : '<?php echo base_url('admin/Produk/hapusData')?>',
        type: 'post',
        data: {id_barang: id_barang},
        success: function(hasil) {
          setTimeout(function(){   location.reload(); }, 1000);
        }
      });
    });

  }

  // === End Function Hapus Produk =======

  </script>
</body>

</html>
