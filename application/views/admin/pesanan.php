<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin OlShopku</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
  <!-- Bootstrap glyphicon CSS -->
  <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('assets/css/font-awesome.css');?>" rel="stylesheet" type="text/css">
  <!-- <link href="vendor/bootstrap/css/fontawesome.min.css" rel="stylesheet" type="text/css"> -->

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url('assets/js/dataTables.bootstrap4.css')?>" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('assets/css/sb-admin.css')?>" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Modal -->
  <div class="modal fade" id="modal_detail_pesanan" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detail Pesanan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- Modal content-->


  <!-- Navigation-->
  <?php $this->load->view('template/navbar.php'); ?>
  <!-- Navigation-->

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Pesanan</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Pesanan</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr align='center'>
                  <th>No Transaksi</th>
                  <th>Tanggal</th>
                  <th>Jumlah Item</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No Transaksi</th>
                  <th>Tanggal</th>
                  <th>Jumlah Item</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
              <tbody>

                <?php foreach ($item as $data_item): ?>
                <tr>
                  <td><?php echo $data_item->no_transaksi ?></td>
                  <td align='center'><?php echo $data_item->tanggal ?></td>
                  <td align='center'><?php echo $data_item->jumlah_item ?></td>
                  <td align='right'>Rp.<?php echo $harga=number_format($data_item->total_bayar,0,",","."); ?></td>
                  <td align='center'><?php echo $data_item->status ?></td>
                  <td align='center' class="none"> <button type="button" class="btn btn-primary" onclick="tampil_detail('<?php echo $data_item->no_transaksi ?>')">Detail</button>
                      <button type="button" class="btn btn-success" onclick="konfirmasi_pembayaran('<?php echo $data_item->no_transaksi ?>')"><span class="glyphicon glyphicon-ok"></span></button>
                  </td>

                </tr>

                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer" >
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Pilih "Logout" untuk keluar.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.easing.min.js');?>"></script>
    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap4.js');?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assets/js/sb-admin.min.js');?>"></script>
    <!-- Custom scripts for this page-->
    <script src="<?php echo base_url('assets/js/sb-admin-datatables.min.js');?>"></script>
  </div>
</body>

</html>



<script type="text/javascript">
	function tampil_detail(no_transaksi) {
		$.ajax({
      url : '<?php echo base_url('pesanan/tampil_detail')?>',
			type: 'post',
      data: {no_transaksi: no_transaksi},

			success: function(hasil) {
				$response = $(hasil);
        var isi = $response.html();
        $('#modal_detail_pesanan .modal-body').html(isi);
				// // ambil data dari url admin/edit_barang?id_barang=(sekian)
				// var nama_barang = $response.filter('#nama_barang').text();
				// var harga = $response.filter('#harga').text();
				// var jumlah = $response.filter('#jumlah').text();
				// // menampilkan ke modal
				// $('#nama_barang').val(nama_barang);
				// $('#harga').val(harga);
				// $('#jumlah').val(jumlah);
        $('#modal_detail_pesanan').modal('show');
        // alert(hasil);

			}
		});
	}


  function konfirmasi_pembayaran(no_transaksi) {
		$.ajax({
      url : '<?php echo base_url('admin/Pesanan/konfirmasi_pembayaran')?>',
			type: 'post',
      data: {no_transaksi: no_transaksi},

			success: function(hasil) {
        // alert (hasil);

			}
		});
	}

</script>
