<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->develop_key = 'AIzaSyCUOsL-2dcWNjPxgUzOrLCsWIPmr0phMBo';
		$this->client_id = '568962455911-frc10j5ivtidiqa6g0tsukronghl01m4.apps.googleusercontent.com';
		$this->client_secret = 'Q1sPCJ3K8ebSVm2akXZiNBSU';
		$this->client = new Google_Client();
		$this->plus = new Google_Service_Plus($this->client);
		$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
		$this->client->setHttpClient($guzzleClient);
		$this->load->model('m_TokoOnline');
		$this->load->model('data_login');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function sign_up()
	{
		$this->load->view('sign_up');
	}

	public function insert()
	{
		$post = $this->input->post();
		$this->session->set_userdata($post);
		$this->data_login->insert_member($post);
		redirect(base_url('login'));
	}

	function aksi_login()
	{
		$email = $_POST['email'];
		$password = $_POST['password'];
		$user = $this->db->get_where('tb_user', array('email' => $email,'hak_akses' => 'member'))->row_array();
		$enpass=$this->encryption->decrypt($user['password']);
		if ($user) {
			if ($this->encryption->decrypt($user['password']) == $password){
				$data_session = array(
					'id_user' => $user['id_user'],
					'email' => $user['email'],
					'hak_akses' => $user['hak_akses'],
				);
				$this->session->set_userdata($data_session);
				switch ($user['hak_akses']) {
					case 'admin':
					$url = base_url('admin/home');
					break;
					case 'member':
					$url = base_url('home');
					break;
					default:
					# code...
					break;
				}
				redirect($url);
			}else {
				redirect(base_url('login'));
			}
		}
	}

	function login_g()
	{
		$OAUTH2_CLIENT_ID = '568962455911-frc10j5ivtidiqa6g0tsukronghl01m4.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'Q1sPCJ3K8ebSVm2akXZiNBSU';
		$client = new Google_Client();
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->addScope('https://www.googleapis.com/auth/plus.login');
		$client->addScope('https://www.googleapis.com/auth/userinfo.profile');
		$client->addScope('https://www.googleapis.com/auth/userinfo.email');
		$client->setAccessType('offline');
		$client->setApprovalPrompt('force');
		$redirect = base_url('login/login_g');
		$client->setRedirectUri($redirect);
		if (isset($_GET['code']))
		{
			// matikan ssl
			$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
			$client->setHttpClient($guzzleClient);
			$authenticate = $client->authenticate($_GET['code']);

			$email = $this->data_login->getUser($authenticate['access_token']);
			$user = $this->db->get_where('tb_user', array('email' => $email['email'],'hak_akses' => 'member'))->row_array();
			$userInfo = array('pembeli' => $user );
			if($user) {
				$data_session = array(
					'id_user' => $user['id_user'],
					'email' => $user['email'],
					'hak_akses' => $user['hak_akses'],
				);
				$this->session->set_userdata($data_session);
				$msg = bin2hex($user['email']);
				redirect(base_url('home').'?m='.$msg);
			} else{
				$data = array(
					'username' => $email['name'],
					'email' => $email['email'],
					'hp'		=> '0',
					'alamat'		=> '-',
					'password' 		=> '-',
					'sumber'		=> 'google',
					'hak_akses'		=> 'member'
				);
				$this->db->insert('tb_user',$data);
				$id_user = $this->db->insert_id();
				$data_session = array(
					'id_user' => $id_user,
					'email' => $email['email'],
					'hak_akses' => 'member',
				);
				$this->session->set_userdata($data_session);
				$msg = bin2hex($email['email']);
				//tambahin ke session
				redirect(base_url('home').'?m='.$msg);
			}
		}else{
			$state = mt_rand();
			$client->setState($state);
			$_SESSION['state'] = $state;
			$authUrl = $client->createAuthUrl();
			redirect($authUrl);
		}
	}

	public function logout(){
		session_destroy();
		redirect();
	}

}
