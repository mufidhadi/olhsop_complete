<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		cek_auth();
		$data['email'] = $_SESSION['email'];
		$data['item'] = $this->m_TokoOnline->tampil('tb_barang');
		$this->load->view('home',$data);
	}

	public function logout(){
		session_destroy();
		redirect();
	}
}
