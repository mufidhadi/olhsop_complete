<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		cek_auth();
		$data['email'] = $_SESSION['email'];
		$id_barang = $this->input->get('id_barang');
		$data = $this->m_TokoOnline->cari('tb_barang','id_barang',$id_barang);
		$this->load->view('produk',$data);
	}

	function tambah_keranjang(){
		$id_barang = $this->input->post('id_barang');
		$harga = $this->input->post('harga');
		$nama_item= $this->input->post('nama');
		$data = array(
			'id'      => $id_barang,
			'qty'     => 1,
			'price'   => $harga,
			'name'    => $nama_item,
			'coupon'         => 'XMAS-50OFF'
		);
		$this->cart->insert($data);
	}

	function isi_keranjang(){
		print_r($this->cart->contents());
	}

}
