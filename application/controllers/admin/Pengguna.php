<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		$data['item'] = $this->m_TokoOnline->tampil('tb_user');
		$this->load->view('admin/pengguna',$data);
	}

	public function tampilUbahData()
	{
		$id_user = $this->input->post('id_user');
		$data = $this->m_TokoOnline->cari('tb_user','id_user',$id_user);

		echo '<div id="id_user">'.$data->id_user.'</div>';
		echo '<div id="username">'.$data->username.'</div>';
		echo '<div id="email">'.$data->email.'</div>';
		echo '<div id="alamat">'.$data->alamat.'</div>';
		echo '<div id="no_hp">'.$data->hp.'</div>';
	}

	public function ubahData(){
		$id_user = $this->input->post('id_user');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$this->m_TokoOnline->update_pengguna($id_user,$username,$email,$alamat,$no_hp);
		redirect(base_url('pengguna'));
	}

	public function hapusData(){
		$id_user = $this->input->post('id_user');
		$this->m_TokoOnline->hapus('tb_user','id_user',$id_user);
	}

}
