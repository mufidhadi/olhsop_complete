<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		$data['item'] = $this->m_TokoOnline->tampil_approv('tb_transaksi');
		$this->load->view('admin/pesanan',$data);
	}

	public function tampil_detail(){
		$no_transaksi = $this->input->post('no_transaksi');
		$data['detail'] = $this->m_TokoOnline->get_detail_pesanan($no_transaksi);
		$this->load->view('detail_transaksi',$data);
	}

	public function konfirmasi_pembayaran(){
		$no_transaksi = $this->input->post('no_transaksi');
		$this->m_TokoOnline->konfirmasi_pembayaran($no_transaksi);
		redirect();
	}

}
