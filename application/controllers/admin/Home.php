<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		cek_auth();
		$data['pengguna']=$this->m_TokoOnline->tampil_count('tb_user');
		$data['item']=$this->m_TokoOnline->tampil_count('tb_barang');
		$data['transaksi']=$this->m_TokoOnline->cari('tb_transaksi','status','Pending');
		// print_r(count($data['pengguna']);
		$this->load->view('admin/home',$data);
	}

}
