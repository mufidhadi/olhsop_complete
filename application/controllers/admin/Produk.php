<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		$data['item'] = $this->m_TokoOnline->tampil('tb_barang');
		$this->load->view('admin/produk',$data);
	}

	// Function upload gambar
	public function do_upload()
	{
		$config['upload_path']          = './assets/gambar/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100000000;
		$this->load->library('upload', $config);
		// Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo "<pre>";
			var_dump($error);
			die();
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$nama_file = $data['upload_data']['file_name'];
			$barang = array(
				'nama'		=> $this->input->post('nama'),
				'deskripsi'		=> $this->input->post('deskripsi'),
				'harga'	=> $this->input->post('harga'),
				'gambar'	=> 'assets/gambar/'.$nama_file
			);
			$this->m_TokoOnline->insert_produk($barang);
		}
		redirect(base_url('admin/Produk/'));
	}

	public function tampilUbahData()
	{
		$id_barang = $this->input->post('id_barang');
		$data = $this->m_TokoOnline->cari('tb_barang','id_barang',$id_barang);
		echo '<div id="id_barang">'.$data->id_barang.'</div>';
		echo '<div id="nama">'.$data->nama.'</div>';
		echo '<div id="harga">'.$data->harga.'</div>';
		echo '<div id="deskripsi">'.$data->deskripsi.'</div>';
		echo '<div id="gambar">'.$data->gambar.'</div>';
	}

	public function ubahData(){
		$id_barang = $this->input->post('id_barang');
		$nama = $this->input->post('ubah_nama');
		$deskripsi = $this->input->post('ubah_deskripsi');
		$harga = $this->input->post('ubah_harga');
		$cek = $this->input->post('ubah_gambar');
		if($cek==''){
			$gambar = $this->input->post('ubah_gambar2');
		}else{
			$gambar = 'assets/gambar/'.$this->input->post('ubah_gambar');
		}
		$this->m_TokoOnline->update_barang($id_barang,$nama,$deskripsi,$harga,$gambar);
		redirect(base_url('admin_produk'));
	}

	public function hapusData(){
		$id_barang = $this->input->post('id_barang');
		$data = $this->m_TokoOnline->hapus('tb_barang','id_barang',$id_barang);
		redirect(base_url('admin_produk'));
	}

}
