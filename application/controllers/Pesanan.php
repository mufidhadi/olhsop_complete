<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		$id_user = $_SESSION['id_user'];
		$data['email'] = $_SESSION['email'];
		$data['item'] = $this->m_TokoOnline->list_order($id_user);
		$this->load->view('pesanan',$data);
	}

	public function tampil_detail(){
		$no_transaksi = $this->input->post('no_transaksi');
		$data['email'] = $_SESSION['email'];
		$data['detail'] = $this->m_TokoOnline->get_detail_pesanan($no_transaksi);
		$this->load->view('detail_transaksi',$data);
	}

}
