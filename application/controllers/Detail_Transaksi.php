<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_Transaksi extends CI_Controller {

	function __construct(){
	 	parent::__construct();
	  $this->load->model('m_TokoOnline');
 	}

	public function index()
	{
		$this->load->view('detail_transaksi');
	}
	
}
