<?php defined('BASEPATH') or exit('No direct script access allowed');

class m_TokoOnline extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function tampil($table){
    $query = $this->db->get($table);
    return $query->result();
  }

  public function tampil_approv($table)
  {
    $this->db->where('status','Pending');
    $query = $this->db->get($table);
    return $query->result();
  }

  public function tampil_count($table){
    $query = $this->db->get($table);
    return $query->num_rows();
  }

  public function cari($table,$kolom,$nilaikolom){
    $this->db->from($table);
    $this->db->where($kolom, $nilaikolom);
    return $this->db->get()->row();
  }

  public function list_order($id_user){
    $this->db->from('tb_transaksi');
    $this->db->where('id_user', $id_user);
    $query= $this->db->get();
    return $query->result();
  }

  public function get_detail_pesanan($no_transaksi){
    $query=$this->db->query("select * FROM tb_detail_transaksi inner join tb_barang on tb_detail_transaksi.id_barang=tb_barang.id_barang where tb_detail_transaksi.no_transaksi='$no_transaksi'");
    return $query->result();
  }

  public function tampil_data_login($email,$password){
    $this->db->select('*');
    $this->db->from('tb_user');
    $this->db->where('email', $email);
    $this->db->where('password',$password);
    return $this->db->get()->row();
  }

  public function autonumber(){
    $this->db->select("no_transaksi");
    $this->db->order_by("no_transaksi", "desc");
    $this->db->limit(1);
    $this->db->from("tb_transaksi");
    $query = $this->db->get();
    $rslt = $query->result_array();
    $total_rec = $query->num_rows();
    if ($total_rec == 0) {
      $nomor = 1;
    } else {
      $nomor = intval(substr($rslt[0]["no_transaksi"],strlen("TR"))) + 1;
    }
    $angka = "TR".$nomor;
    return $angka;
  }

  public function insert_transaksi($data){
    $this->db->insert('tb_transaksi', $data);
  }

  public function insert_produk($data){
    $this->db->insert('tb_barang', $data);
  }

  public function insert_detail_transaksi($data){
    $this->db->insert('tb_detail_transaksi', $data);
  }

  public function konfirmasi_pembayaran($no_transaksi){
    $this->db->where("no_transaksi", $no_transaksi);
    $this->db->set("status", 'Approved');
    $this->db->update("tb_transaksi");
  }

  public function update_barang($id_barang,$nama,$deskripsi,$harga,$gambar){
    $this->db->where("id_barang", $id_barang);
    $this->db->set("nama", $nama);
    $this->db->set("deskripsi", $deskripsi);
    $this->db->set("harga", $harga);
    $this->db->set("gambar", $gambar);
    $this->db->update("tb_barang");
  }

  public function update_pengguna($id_user,$username,$email,$alamat,$no_hp){
    $this->db->where("id_user", $id_user);
    $this->db->set("username", $username);
    $this->db->set("email", $email);
    $this->db->set("alamat", $alamat);
    $this->db->set("hp", $no_hp);
    $this->db->update("tb_user");
  }

  public function hapus($table,$kolom,$nilaikolom){
    $this->db->where($kolom, $nilaikolom);
    $this->db->delete($table);
  }

}
