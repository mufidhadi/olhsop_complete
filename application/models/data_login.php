<?php
defined('BASEPATH') or exit('No direct script access allowed');

class data_login extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  function insert_member($post)
  {
    $cek_email = $this->db->get_where('tb_user', array('email' => $post['email']))->num_rows();
    if ($cek_email > 0)
    {
      $msg = bin2hex('Maaf, email sudah terdaftar, gunakan email lain untuk mendaftar');
      redirect(base_url('signup').'?status=500&msg='.$msg);
    } else {
      $data = array(
        'username' 			=> $post['username'] ,
        'hp'		=> $post['hp'],
        'alamat'		=> $post['alamat'],
        'email' 		=> $post['email'],
        'password' 		=> $this->encryption->encrypt($post['password']),
        'sumber'		=> 'email',
        'hak_akses'		=> 'member',
      );
      $this->db->insert('tb_user',$data);
      return $data;
    }
  }


  function getUser($token)
  {
    // ambil informasi profil user
    $ch = curl_init();
    $url = 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json';
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer '.$token.'',
      'Accept: application/json',
      'Content-Type: application/json',
    ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    if ($result === FALSE) {
      echo "ada error curl berikut : ".curl_error($ch);
    }
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $bentuk_json = json_decode($result,TRUE);
    return $bentuk_json;
  }

  function getEmail($access_token)
  {
    $ch = curl_init();
    $url = 'https://www.googleapis.com/plus/v1/people/me';
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer '.$access_token.'',
      'Accept: application/json',
      'Content-Type: application/json',
    ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    if ($result === FALSE) {
      echo "ada error curl berikut : ".curl_error($ch);
    }
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $bentuk_json = json_decode($result,TRUE);
    $email = $bentuk_json['emails'][0]['value'];
    return $email;
  }

  public function getUserInfo()
  {
    $access_token = $this->access_token;
    $ch = curl_init();
    $url = 'https://graph.facebook.com/v2.12/me?access_token='.$access_token;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/json',
    ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    if ($result === FALSE) {
      echo "ada error curl berikut : ".curl_error($ch);
    }
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $bentuk_json = json_decode($result,TRUE);
    return $bentuk_json;
  }

}
